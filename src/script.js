
var firstCards=[];
var secondCards =[];

var trys = 0;
var cardsFlipped = 0;
var firstGroupCardsFlipped = 0;
var secondGroupCardsFlipped = 0;

var matchedNumber = 0;

var firstFlippedSrc;
var secondFlippedSrc;

var wrongAudio = new Audio('./sound/wrong.mp3');
var correctAudio = new Audio('./sound/correct.mp3');
var selectAudio = new Audio('./sound/selection.mp3');

var playSound;


$('#playBTN').click(startTheGame);
$('#optionsBTN').click(goToOptions);
$('#backToMenu').click(goToMenu);

function startTheGame(){
	playSound = document.getElementById('playSound').checked;
	$('#mainMenu').slideUp();
	$('#options').slideUp();
	$('#game').slideDown();
}



function goToOptions()
{
	$('#mainMenu').slideUp();
	$('#options').slideDown();
	$('#game').slideUp();
	
	
	
}

function goToMenu(){
	$('#mainMenu').slideDown();
	$('#options').slideUp();
	$('#game').slideUp();
}

function endGame(){
	$('#triesNumber').html(trys);
	$('#EndGame').slideDown();
	$('#mainMenu').slideUp();
	$('#options').slideUp();
	$('#game').slideUp();
}


function shuffleCards(className)
{
	var imgPath ="img//part1//";
	var imgesGroup = document.getElementsByClassName(className);
	var randomOrder = [0,1,2,3,4,5,6,7];
	randomOrder = randomOrder.sort(function(a, b){return 0.5 - Math.random()});
	
	for(i =0;i<imgesGroup.length;i++)
	{
		imgesGroup[i].setAttribute("src",imgPath + randomOrder[i].toString() +".jpg");
	}
}


function startOfGame()
{
		shuffleCards("firstImagesGroup");
		shuffleCards("secondImagesGroup");

		//get first group of cards
		for(i = 1; i<9; i++)
		{
			firstCards[i] = document.getElementById("1-"+i.toString()); 

			//addEventListener click to reveal
			firstCards[i].addEventListener('click',revealFirstCard);

		}
		

		//get first group of cards
		for(i = 1; i<9; i++)
		{
			secondCards[i] = document.getElementById("2-"+i.toString());

			//addEventListener click to reveal
			secondCards[i].addEventListener('click',revealSecondCard);
		}
		


}

function revealFirstCard()  // revel cards with 'group' ture = first group. fasle = second group
{
	if(firstGroupCardsFlipped < 1 && cardsFlipped < 2)
	{
		if(!$(this).hasClass('Mached'))
		{
			$(this).children('.cardFace').css('transform','rotatey(0deg)');
			$(this).children('.cardBack').css('transform','rotatey(180deg)');
			$(this).addClass('active');
			if(playSound){selectAudio.play();}
			
			//get src
			firstFlippedSrc = $(this).children('.cardFace').children('img').attr('src');
			firstGroupCardsFlipped++;
			cardsFlipped++;
			if(cardsFlipped == 2)
			window.setTimeout(compareCards,800);
		}
	}
}

function revealSecondCard()  
{
	if( secondGroupCardsFlipped < 1 && cardsFlipped < 2)
		{
			if(!$(this).hasClass('Mached'))
			{
			$(this).children('.cardFace').css('transform','rotatey(0deg)');
			$(this).children('.cardBack').css('transform','rotatey(180deg)');
			$(this).addClass('active');
			if(playSound){selectAudio.play();}
			//get src
			secondFlippedSrc = $(this).children('.cardFace').children('img').attr('src');
			
			secondGroupCardsFlipped++;
			cardsFlipped++;
			if(cardsFlipped == 2)
			window.setTimeout(compareCards,800);
		}
	}

}


function compareCards()
{
	
	if(cardsFlipped == 2)
	{
		

			if(firstFlippedSrc == secondFlippedSrc)
			{
				
					matchedNumber++;
					
					
					cardsFlipped=0;
					if(playSound){correctAudio.play()}
					firstGroupCardsFlipped = 0;
					secondGroupCardsFlipped = 0;
					$('.active').addClass('Mached');
					$('.active').removeClass('active');
					if(matchedNumber == 8)
						endGame();
				
					}else{
					$('.active').children('.cardFace').css('transform','rotatey(180deg)');
					$('.active').children('.cardBack').css('transform','rotatey(0deg)'); 
					
					if(playSound){wrongAudio.play()}
					
					
					trys++;
					cardsFlipped=0;
					firstGroupCardsFlipped = 0;
					secondGroupCardsFlipped = 0;

					$('.active').addClass('');
					$('.active').removeClass('active');
			}
	}
	
}



startOfGame();





